from django.urls import path
from .views import *

urlpatterns = [
    path('user/wishlist', WishlistView.as_view()),
]
