from django.db import models
from django.contrib.auth.models import User
from products.models import *

# Create your models here.


class UserWishList(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
