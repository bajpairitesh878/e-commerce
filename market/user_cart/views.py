from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from .serializer import *
from .models import *
# Create your views here.


class WishlistView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = WishlistSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)

    def get(self, request):
        query = UserWishList.objects.filter(user=request.user)
        serializer = WishlistDetailSerializer(query, many=True)
        return Response(serializer.data)
