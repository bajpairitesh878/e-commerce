from rest_framework import serializers
from web_api.serializers import *
from .models import *


class WishlistSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserWishList
        fields = "__all__"


class WishlistDetailSerializer(serializers.ModelSerializer):
    product = ProductDetailSerializer()

    class Meta:
        model = UserWishList
        fields = "__all__"
