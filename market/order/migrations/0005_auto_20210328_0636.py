# Generated by Django 3.1.6 on 2021-03-28 06:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0004_auto_20210327_1839'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoiceproductdetail',
            name='total_vendor_price',
            field=models.PositiveIntegerField(blank=True, default=1, null=True),
        ),
        migrations.AddField(
            model_name='invoiceproductdetail',
            name='vendor_price',
            field=models.PositiveIntegerField(default=1),
        ),
    ]
