from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import *
from .serializers import *
from knox.auth import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.views.generic import View


class InvoiceDetailView(APIView):
    authentication_classes = (TokenAuthentication, )

    def get(self, request, id, formate=None):
        query = OrderManagement.objects.filter(
            user=id).order_by('-id')
        serializer = InvoiceSerializer(query, many=True)
        return Response(serializer.data)


class CustomerAddressView(APIView):
    def get(self, request, id, formate=None):
        query = CustomerDetail.objects.get(user=id)
        serializer = CustomerAddressSerializer(query)
        return Response(serializer.data)

    def post(self, request, formate=None):
        print(request.data)
        query = CustomerDetail.objects.get(user=request.data['user'])
        if query:
            serializer = CustomerAddressSerializer(query, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors)
        else:
            serializer = CustomerAddressSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors)
