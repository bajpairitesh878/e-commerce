from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from .models import *
from .render import Render


class InvoiceListView(View):

    def get(self, request):
        if request.user.is_superuser:
            invoice_data = OrderManagement.objects.all().order_by('-id')
            print(invoice_data)
            return render(request, 'order_management/InvoiceList.html', {'invoice_list': invoice_data})
        else:
            return HttpResponse('You Are not Authorized for this Call.')


class AdminDashBoardView(View):
    def get(self, request):
        if request.user.is_superuser:
            return render(request, 'order_management/DashboardCommon.html')
        else:
            return HttpResponse('You Are not Authorized for this Call.')


class InvoiceView(View):
    def get(self, request, id):
        if request.user.is_superuser:
            invoice_data = OrderManagement.objects.get(id=id)
            sub_total = 0
            for item in invoice_data.product_detail.all():
                sub_total += item.total
            tax = {}
            total = 0

            if invoice_data.gst_payable == 'yes':
                if invoice_data.customer.state.lower() == invoice_data.CompanyDetail.company_state.lower():
                    tax['CGST'] = sub_total * 9 / 100
                    tax['SGST'] = sub_total * 9 / 100
                else:
                    tax['IGST'] = sub_total * 18 / 100
                total += sub_total + (sub_total*18/100)
            else:
                total += sub_total

            return render(request, 'order_management/invoice.html', {'invoice_data': invoice_data, 'sub_total': sub_total, 'tax': tax, 'total': total})
        else:
            return HttpResponse('You Are not Authorized for this Call.')


class ApiInvoiceView(View):
    def get(self, request, id):
        if id:
            invoice_data = OrderManagement.objects.get(id=id)
            sub_total = 0
            for item in invoice_data.product_detail.all():
                sub_total += item.total
            tax = {}
            total = 0

            if invoice_data.gst_payable == 'yes':
                if invoice_data.customer.state.lower() == invoice_data.CompanyDetail.company_state.lower():
                    tax['CGST'] = sub_total * 9 / 100
                    tax['SGST'] = sub_total * 9 / 100
                else:
                    tax['IGST'] = sub_total * 18 / 100
                total += sub_total + (sub_total*18/100)
            else:
                total += sub_total

            return render(request, 'order_management/invoice.html', {'invoice_data': invoice_data, 'sub_total': sub_total, 'tax': tax, 'total': total})
        else:
            return HttpResponse('You Are not Authorized for this Call.')
