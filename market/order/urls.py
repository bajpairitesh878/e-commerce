from django.urls import path, include
from .views import *
from .apiviews import *
# frontend url

web_api = [
    path('api/v1/invoice/<int:id>', InvoiceDetailView.as_view()),
    path('api/v1/invoice/print/<int:id>', ApiInvoiceView.as_view()),
    path('api/v1/user/address/<int:id>', CustomerAddressView.as_view()),
    path('api/v1/user/address/update', CustomerAddressView.as_view()),
]

# Backend url

urlpatterns = [
    path('admin/invoice/list', InvoiceListView.as_view()),
    path('admin/dashboard', AdminDashBoardView.as_view()),
    path('admin/print/invoice/<int:id>', InvoiceView.as_view()),
] + web_api
