from rest_framework import serializers
from .models import *


class OrderProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvoiceProductDetail
        fields = ('product_name', 'product_description', 'quantity', 'total')


class InvoiceSerializer(serializers.ModelSerializer):
    product_detail = OrderProductSerializer(read_only=True, many=True)

    class Meta:
        model = OrderManagement
        fields = ('product_detail', 'payment_status',
                  'generate_invoice', 'delivery_date', 'gst_payable', 'id')


class CustomerAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerDetail
        fields = '__all__'
