from django.db import models
from phone_field import PhoneField
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.html import mark_safe
from django.contrib.auth.models import User
# Create your models here.


def increment_invoice_number():
    last_invoice = OrderManagement.objects.all().order_by('id').last()
    if not last_invoice:
        return 'RB0001'
    invoice_no = last_invoice.invoice_no
    invoice_int = int(invoice_no.split('RB')[-1])
    new_invoice_int = invoice_int + 1
    new_invoice_no = 'RB000' + str(new_invoice_int)
    return new_invoice_no


class CustomerDetail(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=100, default='')
    streat_detail = models.TextField(default='', null=True, blank=True)
    city = models.CharField(default='', null=True, blank=True, max_length=100)
    state = models.CharField(default='Delhi', null=True,
                             blank=True, max_length=100)
    phone = PhoneField(blank=True, null=True)
    email = models.EmailField(null=True, blank=True, default='')
    gstn = models.CharField(max_length=150, null=True, blank=True, default='')

    def __str__(self):
        return self.name


class CompanyDetail(models.Model):
    company_name = models.CharField(max_length=100, default='')
    company_gstn = models.CharField(
        max_length=150, null=True, blank=True, default='')
    streat_detail = models.TextField(default='', null=True, blank=True)
    company_city = models.CharField(
        default='', null=True, blank=True, max_length=100)
    company_state = models.CharField(default='Delhi', null=True,
                                     blank=True, max_length=100)
    company_phone = PhoneField(blank=True, null=True)
    company_email = models.EmailField(null=True, blank=True, default='')
    invoice_logo = models.ImageField()

    def __str__(self):
        return self.company_name


class InvoiceProductDetail(models.Model):
    product_name = models.CharField(max_length=150, null=True, blank=True)
    product_description = models.CharField(
        max_length=150, null=True, blank=True)
    quantity = models.PositiveSmallIntegerField(
        default=1, null=True, blank=True)
    unique_price = models.PositiveIntegerField(default=1)
    total = models.PositiveIntegerField(default=1, null=True, blank=True)
    vendor_price = models.PositiveIntegerField(default=1)
    total_vendor_price = models.PositiveIntegerField(
        default=1, null=True, blank=True)

    def __str__(self):
        return self.product_name

    def save(self, *args, **kwargs):
        self.total = self.unique_price * self.quantity
        self.total_vendor_price = self.vendor_price * self.quantity
        return super().save(*args, **kwargs)


class OrderManagement(models.Model):
    GST_PAY = [
        ('yes', 'YES'),
        ('no', 'NO')
    ]
    BIL_PAY = [
        ('paid', 'PAID'),
        ('unpaid', 'UNPAID')
    ]
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True)
    customer = models.ForeignKey(CustomerDetail, on_delete=models.CASCADE)
    CompanyDetail = models.ForeignKey(CompanyDetail, on_delete=models.CASCADE)
    product_detail = models.ManyToManyField(InvoiceProductDetail)
    additional_notes = models.TextField(
        max_length=1500, null=True, blank=True, default='')
    gst_payable = models.CharField(
        choices=GST_PAY, default='no', max_length=100)
    invoice_no = models.CharField(
        max_length=500, default=increment_invoice_number, null=True, blank=True)
    invoice_date = models.DateField()
    billing_date = models.DateField()
    delivery_date = models.DateField()
    payment_status = models.CharField(
        choices=BIL_PAY, default='unpaid', max_length=100)
    generate_invoice = models.CharField(
        max_length=300, null=True, blank=True, default='')

    def invoice_generate(self):
        return mark_safe('<a target="_blank" href="%s">%s</a>' % (self.generate_invoice, 'Generate Invoice'))
    invoice_generate.allow_tags = True


@receiver(post_save, sender=OrderManagement)
def my_handler(sender, instance, **kwargs):
    invoice_url = f'/order/admin/print/invoice/{instance.id}'
    print(invoice_url)
    order = OrderManagement.objects.filter(
        id=instance.id).update(generate_invoice=invoice_url)
    print(order)
