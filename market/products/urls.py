"""product URL Configuration

This file contains all the url of website template and views.

"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from .views import *

urlpatterns = [
    path('', HomeView.as_view()),
    path('category', CategoryPageView.as_view()),
]
