from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse
from ckeditor.fields import RichTextField
from django.utils.html import mark_safe
from colorfield.fields import ColorField

# Create your models here.


class Banner(models.Model):
    slider_banner = models.FileField(
        blank=True, null=True, verbose_name='Banner', upload_to='banner')
    banner_url = models.CharField(
        blank=True, null=True, max_length=50, verbose_name='Banner Redirect Url',)
    banner_heading = models.CharField(
        max_length=500, blank=True, null=True, verbose_name='Banner Display Heading')
    banner_content = RichTextField(
        null=True, blank=True, verbose_name='Product Description', default='Banner Text')
    banner_heading_color = ColorField(default='#FF0000')
    banner_content_color = ColorField(default='#FF0000')
    status = models.BooleanField(default=False, verbose_name='Banner Active')

    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="150" height="150" />' % (self.slider_banner))

    image_tag.short_description = 'Home Page Banner'


class Category(models.Model):
    category_names = models.CharField(
        max_length=255, blank=True, null=True, verbose_name='Category')
    slug = models.SlugField(null=False, unique=True,
                            verbose_name='Product Url')
    category_image = models.ImageField(
        null=True, blank=True, upload_to='category/image')
    parent_category = models.ForeignKey(
        'self', on_delete=models.CASCADE, default=1)
    meta_title = models.CharField(
        null=True, blank=True, default='', verbose_name='Meta Title', max_length=50)
    meta_keywords = models.TextField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.category_names

    def get_absolute_url(self):
        return reverse('category_detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):  # new
        if not self.slug:
            self.slug = slugify(self.category_names)

        if not self.meta_title:
            self.meta_title = self.category_names
        return super().save(*args, **kwargs)


class ProductAttribute(models.Model):
    PRODUCT_RAM = (
        ('4 GB', '4 GB'),
        ('8 GB', '8 GB'),
        ('12 GB', '12 GB'),
        ('16 GB', '16 GB'),
    )

    PRODUCT_HARDWARE = (
        ('No', 'No'),
        ('120 GB', '120 GB'),
        ('240 GB', '240 GB'),
        ('520 GB', '520 GB'),
        ('1 TB', '1 TB'),
    )

    ram = models.CharField(choices=PRODUCT_RAM,
                           max_length=200, verbose_name='Product Ram')
    ssd = models.CharField(choices=PRODUCT_HARDWARE, max_length=200,
                           verbose_name='Product ssd')
    hdd = models.CharField(choices=PRODUCT_HARDWARE, max_length=200,
                           verbose_name='Product hdd')


class Product(models.Model):
    PRODUCT_STATUS = (
        ('active', 'Active'),
        ('disable', 'Disable'),
    )
    product_category = models.ManyToManyField(
        Category)
    product_attribute = models.ManyToManyField(
        ProductAttribute)
    product_name = models.CharField(
        max_length=255, null=True, blank=True, verbose_name='Product Name', default=0)
    product_quantity = models.IntegerField(default=1)
    product_price_actual = models.IntegerField(
        null=True, blank=True, verbose_name='Actual Product Price', default=0)
    product_price_offer = models.IntegerField(
        null=True, blank=True, verbose_name='Product Price Offer', default=0)
    price_offer = models.DecimalField(
        null=True, blank=True, verbose_name='Offer Price', default=0, max_digits=10, decimal_places=2)
    slug = models.SlugField(null=False, unique=True,
                            verbose_name='Product Url')
    product_description = RichTextField(
        null=True, blank=True, verbose_name='Product Description', default='')
    thumbnail_image = models.FileField(
        null=True, blank=True, upload_to='product')
    small_image = models.FileField(
        null=True, blank=True, upload_to='product')
    scrach_image = models.FileField(
        null=True, blank=True, upload_to='product')
    full_image = models.FileField(
        null=True, blank=True, upload_to='product')
    status = models.CharField(choices=PRODUCT_STATUS, max_length=200,)
    featured = models.BooleanField(
        default=False, verbose_name='Is Feature', null=True, blank=True)
    best_seller = models.BooleanField(
        default=False, verbose_name='Is Feature', null=True, blank=True)
    arrivel_date = models.DateField(
        verbose_name='Product Arriving Year', blank=True, null=True)
    product_add_date = models.DateField(auto_now=True,)
    product_spacification = RichTextField(
        null=True, blank=True, verbose_name='Product Spacification', default='')
    meta_title = models.CharField(
        null=True, blank=True, default='', verbose_name='Meta Title', max_length=150)
    meta_keywords = models.TextField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.product_name

    def get_absolute_url(self):
        return reverse('article_detail', kwargs={'slug': self.product_name})

    def price_display_offer(self):
        return "₹%s" % self.price_offer

    def save(self, *args, **kwargs):  # new
        if not self.slug:
            self.slug = slugify(self.product_name)
        if not self.price_offer:
            self.price_offer = self.product_price_actual - self.product_price_offer
        if not self.meta_title:
            self.meta_title = self.product_name

        return super().save(*args, **kwargs)


class LatestCardImage(models.Model):
    card_image = models.FileField(
        blank=True, null=True, verbose_name='Display Image', upload_to='cardimage')
    image_url = models.CharField(
        blank=True, null=True, max_length=50, verbose_name='Image Redirect Url',)
    image_heading = models.CharField(
        max_length=300, blank=True, null=True, verbose_name='Display Heading')
    image_content = models.CharField(
        max_length=300, blank=True, null=True, verbose_name='Display Content')
    image_heading_color = ColorField(default='#FF0000')
    image_content_color = ColorField(default='#FF0000')

    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="150" height="150" />' % (self.card_image))

    image_tag.short_description = 'Home Page Card Image'


class NewArrivalSlider(models.Model):
    slider_banner = models.FileField(
        blank=True, null=True, verbose_name='Banner', upload_to='banner')
    banner_url = models.CharField(
        blank=True, null=True, max_length=50, verbose_name='Banner Redirect Url',)
    banner_heading = models.CharField(
        max_length=500, blank=True, null=True, verbose_name='Banner Display Heading')
    banner_content = RichTextField(
        null=True, blank=True, verbose_name='Banner Description', default='Banner Text')
    banner_heading_color = ColorField(default='#FF0000')
    banner_content_color = ColorField(default='#FF0000')

    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="150" height="150" />' % (self.slider_banner))

    image_tag.short_description = 'Home Page New Arrival Banner'
