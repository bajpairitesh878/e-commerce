from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from .models import *

# Create your views here.


def index(request):
    # return HttpResponse('Home Page')
    return render(request, 'static/home.html')


class HomeView(View):

    def get(self, request):
        products_data = Product.objects.filter(
            status='active').order_by('-id')[0:8]
        banners = Banner.objects.all()
        new_arrival_slider = NewArrivalSlider.objects.all()
        cards = LatestCardImage.objects.all()
        data = {'products': products_data, 'new_arrival_sliders': new_arrival_slider[1::], 'new_arrivel_first_banner': new_arrival_slider[
            0], 'cards': cards, 'banners': banners[1::], 'first_banner': banners[0], 'cards': cards}
        return render(request, 'static/home.html', data)


class CategoryPageView(View):

    def get(self, request):
        return render(request, 'static/category.html')
