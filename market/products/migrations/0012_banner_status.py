# Generated by Django 3.1.6 on 2021-03-06 22:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0011_newarrivalslider'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='status',
            field=models.BooleanField(default=False, verbose_name='Banner Active'),
        ),
    ]
