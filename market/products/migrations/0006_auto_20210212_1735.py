# Generated by Django 3.1 on 2021-02-12 17:35

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0005_banner'),
    ]

    operations = [
        migrations.RenameField(
            model_name='banner',
            old_name='first_banner',
            new_name='slider_banner',
        ),
        migrations.RemoveField(
            model_name='banner',
            name='fourth_banner',
        ),
        migrations.RemoveField(
            model_name='banner',
            name='second_banner',
        ),
        migrations.RemoveField(
            model_name='banner',
            name='third_banner',
        ),
        migrations.AddField(
            model_name='banner',
            name='banner_content',
            field=ckeditor.fields.RichTextField(blank=True, default='Banner Text', null=True, verbose_name='Product Description'),
        ),
        migrations.AddField(
            model_name='banner',
            name='banner_heading',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Banner Display Heading'),
        ),
        migrations.AddField(
            model_name='banner',
            name='banner_url',
            field=models.URLField(blank=True, null=True, verbose_name='Banner Redirect Url'),
        ),
    ]
