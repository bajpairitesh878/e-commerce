# Generated by Django 3.1 on 2021-02-12 17:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0006_auto_20210212_1735'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='banner_url',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Banner Redirect Url'),
        ),
    ]
