from django import forms


class MyForm(forms.ModelForm):
    class Meta:
        widgets = {
            'my_field': forms.TextInput(attrs={'size': '60'}),
        }
