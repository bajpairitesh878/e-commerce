from django.contrib import admin
from django.contrib import messages
from .models import *
from .forms import *
from random import randint
# Register your models here


class MyModelAdmin(admin.ModelAdmin):
    form = MyForm


class CategoryAdminView(admin.ModelAdmin):
    list_display = ('category_names', 'slug', 'id')
    list_filter = ('category_names', 'slug')
    search_fields = ('category_names', 'slug')


class ProductAttributeAdminView(admin.ModelAdmin):
    list_display = ('ram', 'ssd', 'hdd', 'id')
    list_filter = ('ram', 'ssd', 'hdd', 'id')
    search_fields = ('ram', 'ssd', 'hdd', 'id')


class ProductAdminView(admin.ModelAdmin):
    list_display = ('product_name', 'price_display_offer',
                    'status', 'slug', 'id')
    list_filter = ('product_name',
                   'status', 'slug', 'id')
    search_fields = ('product_name',
                     'status', 'slug', 'id')

    def active(self, obj):
        return obj.status == 'active'

    def make_active(modeladmin, request, queryset):
        queryset.update(status='active')
        messages.success(
            request, "Selected Record(s) Marked as Active Successfully !!")

    def make_inactive(modeladmin, request, queryset):
        queryset.update(status='disable')
        messages.success(
            request, "Selected Record(s) Marked as Inactive Successfully !!")

    def make_duplicate(modeladmin, request, queryset):
        inst = queryset.get()
        obj = Product.objects.get(pk=inst.id)
        obj.pk = None
        obj.slug = obj.slug + '.' + str(randint(0, 10))
        obj.save()
        messages.success(
            request, "Selected Record(s) Duplicate Successfully !!")

    admin.site.add_action(make_active, "Make Active")
    admin.site.add_action(make_inactive, "Make Inactive")
    admin.site.add_action(make_duplicate, "Duplicate One Record")


class BannerAdminView(admin.ModelAdmin):
    list_display = ('id', 'image_tag', 'banner_heading',
                    'banner_url', 'status')
    list_filter = ('id', 'banner_heading', 'banner_url', 'status')
    list_display_links = ('id', 'image_tag', 'banner_heading', 'banner_url')
    search_fields = ('id', 'banner_heading', 'banner_url')


class LatestCardImageAdminView(admin.ModelAdmin):
    list_display = ('id', 'card_image', 'image_url', 'image_heading')
    list_filter = ('id', 'card_image', 'image_heading')
    list_display_links = ('id', 'card_image', 'image_heading')
    search_fields = ('id', 'card_image', 'image_heading')


class NewArrivalSliderAdminView(admin.ModelAdmin):
    list_display = ('id', 'image_tag', 'banner_heading', 'banner_url')
    list_filter = ('id', 'banner_heading', 'banner_url')
    list_display_links = ('id', 'image_tag', 'banner_heading', 'banner_url')
    search_fields = ('id', 'banner_heading', 'banner_url')


admin.site.register(Category, CategoryAdminView)
admin.site.register(ProductAttribute, ProductAttributeAdminView)
admin.site.register(Product, ProductAdminView)
admin.site.register(Banner, BannerAdminView)
admin.site.register(LatestCardImage, LatestCardImageAdminView)
admin.site.register(NewArrivalSlider, NewArrivalSliderAdminView)
