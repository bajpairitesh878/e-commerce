from django.apps import AppConfig


class WebApiConfig(AppConfig):
    name = 'web_api'
    verbose_name = 'Settings'
