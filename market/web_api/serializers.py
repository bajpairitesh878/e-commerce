from rest_framework import serializers
from products.models import *
from .models import *


class HomeBannerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Banner
        fields = '__all__'


class LatestCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = LatestCardImage
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class NewArrivalSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewArrivalSlider
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class CategoryDetailSerializer(serializers.ModelSerializer):
    parent_category = CategorySerializer()

    class Meta:
        model = Category
        fields = '__all__'


class ProductAttributeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductAttribute
        fields = '__all__'


class ProductDetailSerializer(serializers.ModelSerializer):
    product_category = CategorySerializer(many=True)
    product_attribute = ProductAttributeSerializer(many=True)

    class Meta:
        model = Product
        fields = '__all__'


class PriceFilterSerializer(serializers.ModelSerializer):
    class Meta:
        model = PriceFilter
        fields = '__all__'


class SettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicationSetting
        fields = '__all__'


class FooterAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = FooterAddress
        fields = '__all__'


class FooterFollowSerializer(serializers.ModelSerializer):
    class Meta:
        model = FooterFollow
        fields = '__all__'


class FooterCompanyInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = FooterCompanyInfo
        fields = '__all__'


class FooterCategoryInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = FooterCategory
        fields = '__all__'
