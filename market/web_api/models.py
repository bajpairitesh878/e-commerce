from django.db import models
from django.utils.html import mark_safe
from ckeditor.fields import RichTextField, RichTextFormField
# Create your models here.


class PriceFilter(models.Model):
    price_minimum = models.IntegerField(null=True, blank=True, default=0)
    price_maximum = models.IntegerField(null=True, blank=True, default=0)


class ApplicationSetting(models.Model):
    site_logo = models.ImageField(
        verbose_name='Site Header Logo', default='')
    site_logo_footer = models.ImageField(
        verbose_name='Site Footer Logo', default='')
    contact_phone = models.IntegerField(
        verbose_name='Phone Number', null=True, blank='True')
    whatsapp_phone = models.IntegerField(
        verbose_name='Whatsapp Phone Number', null=True, blank='True')
    meta_title = models.CharField(
        null=True, blank=True, default='', verbose_name='Meta Title', max_length=150)
    meta_keywords = models.TextField(
        max_length=200, null=True, blank=True, verbose_name='Meta Description')

    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="80" height="50" />' % (self.site_logo))

    def image_tag2(self):
        return mark_safe('<img src="/media/%s" width="80" height="50" />' % (self.site_logo_footer))

    image_tag.short_description = 'Website Header Logo'
    image_tag2.short_description = 'Website Footer Logo'


class FooterAddress(models.Model):
    title = models.CharField(max_length=100, default='', null=True, blank=True)
    page_content = RichTextField(
        null=True, blank=True, verbose_name='Footer Address Block', default='')


class FooterFollow(models.Model):
    title = models.CharField(max_length=100, default='', null=True, blank=True)
    page_content = RichTextField(
        null=True, blank=True, verbose_name='Footer Follow Us Block', default='')


class FooterCompanyInfo(models.Model):
    title = models.CharField(max_length=100, default='', null=True, blank=True)
    page_content = RichTextField(
        null=True, blank=True, verbose_name='Footer Company Information', default='')


class FooterCategory(models.Model):
    title = models.CharField(max_length=100, default='', null=True, blank=True)
    page_content = RichTextField(
        null=True, blank=True, verbose_name='Footer Category', default='')
