from django.contrib import admin
from .models import *

# Register your models here.


class PriceFilterAdminView(admin.ModelAdmin):
    list_display = ('price_minimum', 'price_maximum')


class SettingsAdminView(admin.ModelAdmin):
    list_display = ('id', 'contact_phone', 'whatsapp_phone',
                    'image_tag', 'image_tag2')


class FooterAdminView(admin.ModelAdmin):
    list_display = ('id', 'title')
    list_display_links = ('id', 'title')


admin.site.register(PriceFilter, PriceFilterAdminView)
admin.site.register(ApplicationSetting, SettingsAdminView)
admin.site.register(FooterAddress, FooterAdminView)
