from django.urls import path, include
from .views import *

urlpatterns = [
    path('home/banner/top', HomeTopBanner.as_view()),
    path('home/image/card', LatestCardImageView.as_view()),
    path('home/product/feature', FeatureProductView.as_view()),
    path('home/product/recent', RecentProductView.as_view()),
    path('home/slider/new-arrival', NewArrivalSliderView.as_view()),
    path('home/header/category', CategoryView.as_view()),
    path('home/header/category/<int:id>', SubCategoryView.as_view()),
    path('category/product/<slug>', CategoryProduct.as_view()),
    path('category/product/plth/<slug>', ProductPriceLtHView.as_view()),
    path('category/product/phtl/<slug>', ProductPriceHtLView.as_view()),
    path('category/product/new/<slug>', ProductNAView.as_view()),
    path('deatil/product/<slug>', ProductDetailView.as_view()),
    path('header/search', ProductSearchView.as_view()),
    path('filter/price', PriceFilterView.as_view()),
    path('filter/price/product', ProductPriceRange.as_view()),
    path('home/product/bestshell', BestSellProductView.as_view()),
    path('app/settings', AppSettingsView.as_view()),
    path('product/related/<slug>', RelatedProduct.as_view()),
    path('footer/address', FooterAddressView.as_view()),
    path('footer/follow', FooterFollowView.as_view()),
    path('footer/company', FooterCompanyInfoView.as_view()),
    path('footer/category', FooterCategoryInfoView.as_view()),
    path('category/current/<slug>', CurrentCategoryInfoView.as_view()),
]
