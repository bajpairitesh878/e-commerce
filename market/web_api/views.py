from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework import status, generics, filters
from rest_framework.response import Response
from .serializers import *
from products.models import*
from .models import *
# Create your views here.


class HomeTopBanner(APIView):
    def get(self, request, form=None):
        active_banners = Banner.objects.filter(status=True)
        serializer = HomeBannerSerializer(active_banners, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class LatestCardImageView(APIView):
    def get(self, request, form=None):
        active_banners = LatestCardImage.objects.all()
        serializer = LatestCardSerializer(active_banners, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class NewArrivalSliderView(APIView):
    def get(self, request, form=None):
        active_slider = NewArrivalSlider.objects.all()
        serializer = NewArrivalSerializer(active_slider, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FeatureProductView(APIView):
    def get(self, request, formate=None):
        feature_product = Product.objects.filter(featured=True)
        serializer = ProductSerializer(feature_product, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class RecentProductView(APIView):
    def get(self, request, formate=None):
        recent_product = Product.objects.order_by('-id')
        serializer = ProductSerializer(recent_product, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class CategoryView(APIView):
    def get(self, request, formate=None):
        category = Category.objects.filter(
            parent_category__category_names='Tesla')
        serializer = CategorySerializer(category, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class SubCategoryView(APIView):
    def get(self, request, id, formate=None):
        category = Category.objects.filter(
            parent_category=id)
        serializer = CategorySerializer(category, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class CategoryProduct(APIView):
    def get(self, request, slug, formate=None):
        category_product = Product.objects.filter(product_category__slug=slug)
        serializer = ProductSerializer(category_product, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ProductDetailView(APIView):
    def get(self, request, slug, formate=None):
        product = Product.objects.filter(slug=slug).last()
        serializer = ProductDetailSerializer(product)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ProductPriceLtHView(APIView):
    def get(self, request, slug, formate=None):
        category_product = Product.objects.filter(
            product_category__slug=slug).order_by('product_price_offer')
        serializer = ProductSerializer(category_product, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ProductPriceHtLView(APIView):
    def get(self, request, slug, formate=None):
        category_product = Product.objects.filter(
            product_category__slug=slug).order_by('-product_price_offer')
        serializer = ProductSerializer(category_product, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ProductNAView(APIView):
    def get(self, request, slug, formate=None):
        category_product = Product.objects.filter(
            product_category__slug=slug).order_by('-id')
        serializer = ProductSerializer(category_product, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ProductSearchView(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['product_name']


class PriceFilterView(generics.ListAPIView):
    queryset = PriceFilter.objects.all()
    serializer_class = PriceFilterSerializer


class ProductPriceRange(APIView):
    def post(self, request):
        product_data = Product.objects.filter(
            product_price_offer__gte=request.data['price_minimum'], product_price_offer__lte=request.data['price_maximum'])
        serializer = ProductSerializer(product_data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class BestSellProductView(APIView):
    def get(self, request, formate=None):
        best_seller = Product.objects.filter(best_seller=True)
        serializer = ProductSerializer(best_seller, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class AppSettingsView(APIView):
    def get(self, request, formate=None):
        settings = ApplicationSetting.objects.all()
        serializer = SettingsSerializer(settings, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class RelatedProduct(APIView):
    def get(self, request, slug, formate=None):
        product_data = Product.objects.get(slug=slug)
        serializer = ProductSerializer(product_data)
        related_product = Product.objects.filter(
            product_category__in=serializer.data['product_category']).exclude(id=serializer.data['id'])
        serializer = ProductSerializer(related_product, many=True)
        return Response(serializer.data)


class FooterAddressView(APIView):
    def get(self, request, formate=None):
        footer_address = FooterAddress.objects.all()
        serializer = FooterAddressSerializer(footer_address, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FooterFollowView(APIView):
    def get(self, request, formate=None):
        footer_follow = FooterFollow.objects.all()
        serializer = FooterFollowSerializer(footer_follow, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FooterCompanyInfoView(APIView):
    def get(self, request, formate=None):
        footer_company = FooterCompanyInfo.objects.all()
        serializer = FooterCompanyInfoSerializer(footer_company, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class FooterCategoryInfoView(APIView):
    def get(self, request, formate=None):
        footer_category = FooterCategory.objects.all()
        serializer = FooterCategoryInfoSerializer(footer_category, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class CurrentCategoryInfoView(APIView):
    def get(self, request, slug, formate=None):
        category = Category.objects.get(slug=slug)
        serializers = CategorySerializer(category)
        return Response(serializers.data)
