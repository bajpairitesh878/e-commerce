from django.contrib import admin
from .models import *

# Register your models here.


class TestimonialAdminView(admin.ModelAdmin):
    list_display = ('id', 'name', 'profession')
    list_display_links = ('id', 'name', 'profession')


class QueryAdminView(admin.ModelAdmin):
    list_display = ('id', 'product', 'name', 'phone')
    list_display_links = ('id', 'product', 'name', 'phone')


admin.site.register(Testimonial, TestimonialAdminView)
admin.site.register(Query, QueryAdminView)
