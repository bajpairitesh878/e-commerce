# Generated by Django 3.1.6 on 2021-03-20 18:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0003_auto_20210320_1823'),
    ]

    operations = [
        migrations.AddField(
            model_name='productreview',
            name='created_date',
            field=models.DateField(auto_now=True),
        ),
        migrations.AddField(
            model_name='query',
            name='created_date',
            field=models.DateField(auto_now=True),
        ),
        migrations.AddField(
            model_name='testimonial',
            name='created_date',
            field=models.DateField(auto_now=True),
        ),
    ]
