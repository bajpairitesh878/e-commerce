# Generated by Django 3.1.6 on 2021-03-20 18:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0019_product_product_spacification'),
        ('contact', '0002_testimonial'),
    ]

    operations = [
        migrations.AddField(
            model_name='query',
            name='email',
            field=models.CharField(blank=True, default='', max_length=50, null=True),
        ),
        migrations.CreateModel(
            name='ProductReview',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default='', max_length=50, null=True)),
                ('email', models.CharField(blank=True, default='', max_length=50, null=True)),
                ('review', models.CharField(blank=True, default='', max_length=500, null=True)),
                ('star_count', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=10, null=True, verbose_name='Offer Price')),
                ('product', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='products.product')),
            ],
        ),
    ]
