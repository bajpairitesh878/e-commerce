from django.template.loader import render_to_string, get_template
from products.models import *
from web_api.serializers import ProductSerializer
from .models import *
from .serializers import *


def product_suggetion(product_id):
    product_id = 1
    product_category = Product.objects.get(id=product_id)
    category = ProductSerializer(product_category).data['product_attribute']
    product_data = Product.objects.filter(product_attribute__in=category)
    serializer = ProductSerializer(product_data, many=True).data
    html_body = render_to_string("template.html",  {'products': serializer})

    return html_body
