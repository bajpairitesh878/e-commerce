from django.db import models
from products.models import *
from ckeditor.fields import RichTextField
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

# Create your models here.


class Query(models.Model):
    product = models.ForeignKey(
        Product, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, null=True, blank=True, default='')
    email = models.CharField(max_length=50, null=True, blank=True, default='')
    phone = models.IntegerField(null=True, blank=True)
    message = models.CharField(
        max_length=1000, null=True, blank=True, default='')
    created_date = models.DateField(auto_now=True)


class Testimonial(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True, default='')
    content = RichTextField(
        null=True, blank=True, verbose_name='Product Description', default='')
    profession = models.CharField(
        max_length=100, null=True, blank=True, default='')
    picture = models.ImageField(verbose_name='User Profile Picture')
    created_date = models.DateField(auto_now=True)

    def __str__(self):
        return self.name


class ProductReview(models.Model):
    product = models.ForeignKey(
        Product, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, null=True, blank=True, default='')
    email = models.CharField(max_length=50, null=True, blank=True, default='')
    review = models.CharField(
        max_length=500, null=True, blank=True, default='')
    star_count = models.DecimalField(
        null=True, blank=True, verbose_name='Offer Price', default=0, max_digits=10, decimal_places=2)
    created_date = models.DateField(auto_now=True)
