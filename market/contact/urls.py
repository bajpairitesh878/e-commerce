from django.urls import path
from .views import *

urlpatterns = [
    path('query/create', QueryView.as_view()),
    path('testimonials', TestimonialsView.as_view()),
    path('review/create', ReviewView.as_view()),
    path('review/<int:id>', ReviewView.as_view()),
    # path('send/mail', Sendemail.as_view()),
]
