from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from .serializers import *
from .wp_message import *
from market.settings import EMAIL_HOST_USER
from django.template import Context
from django.template.loader import render_to_string, get_template
from .algorithm import *
# Create your views here.


class QueryView(APIView):
    def post(self, request, formate=None):
        serializer = QuerySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            try:
                data = send_product_mail(
                    to_email=serializer.data['email'], product_id=serializer.data['product'])
            except:
                print('mail not send')
            return Response(serializer.data)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)


class TestimonialsView(APIView):
    def get(self, request, formate=None):
        query = Testimonial.objects.all()[0:4]
        serializer = TestimonialsSerializer(query, many=True)
        return Response(serializer.data)


class ReviewView(APIView):
    def get(self, request, id, formate=None):
        query = ProductReview.objects.filter(product=id)
        serializer = ReviewSerializer(query, many=True)
        return Response(serializer.data)

    def post(self, request, formate=None):
        serializer = ReviewSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)


def send_product_mail(to_email, product_id):
    html_body = product_suggetion(1)
    msg = EmailMultiAlternatives(subject='subject', from_email=EMAIL_HOST_USER,
                                 to=[to_email], body=html_body)
    msg.attach_alternative(html_body, "text/html")
    msg.send()
    return Response(html_body)
